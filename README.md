# PVDockerOSMesa

This repository uses the gitlab pipeline to build a Docker image building [ParaView](https://github.com/Kitware/ParaView/blob/master/Documentation/dev/build.md).

The build is done with Off-screen (OS) Mesa, so headless rendering is possible. 

The main application of this docker image is to [launch a paraview server on a remote machine and connect to it from within the docker container](https://gitlab.com/bothambrus/pvdocker#connecting-paraview-in-docker-container-to-a-remote-server).

See the [PVDocker](https://gitlab.com/bothambrus/pvdocker) repository for further details on usage. 



